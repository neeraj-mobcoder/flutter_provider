import 'package:flutter/material.dart';
import 'package:flutter_provider/home.dart';
import 'package:flutter_provider/model.dart';
import 'package:flutter_provider/signup.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

final _formKey = GlobalKey<FormState>();
String email = '';

String emailValidator(String value) {
  if (value.isEmpty) {
    return 'Email is Empty';
  } else {
    return null;
  }
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Form(
                key: _formKey,
                child: Column(
                  children: [
                    TextFormField(
                      validator: emailValidator,
                      onChanged: (value) {
                        setState(() {
                          email = value;
                        });
                      },
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    TextFormField(),
                    SizedBox(
                      height: 40,
                    ),
                  ],
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  RaisedButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ChangeNotifierProvider(
                                  create: (context) => LoginModel(),
                                  child: HomePage(email: email))));
                    },
                    child: Text('Login'),
                  ),
                  SizedBox(
                    width: 40,
                  ),
                  RaisedButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => SignUpPage()));
                    },
                    child: Text('SignUp'),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
