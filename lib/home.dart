import 'package:flutter/material.dart';
import 'package:flutter_provider/model.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  final String email;

  const HomePage({Key key, this.email}) : super(key: key);
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();


  }
  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<LoginModel>(context);
    provider.setEmail(widget.email);
    return Consumer<LoginModel>(
      builder: (context,loginModel, _){
        return Scaffold(
          appBar: AppBar(),
          body: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Center(child: Text(loginModel.email)),
              SizedBox(height: 40,),
              Text('Below text is rendering from provider'),
              StatelessView()
            ],
          ),
        );
      },
    );
  }
}


class StatelessView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Consumer<LoginModel>(

        builder: (context,loginModel,_){
          return Text(loginModel.email);
        },
          child: Text('cvfdd')),
    );
  }
}

