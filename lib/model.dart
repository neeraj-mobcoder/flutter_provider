import 'package:flutter/cupertino.dart';

class LoginModel extends ChangeNotifier {
  String email;

  setEmail(String email) {
    this.email = email;
    notifyListeners();
  }
}
